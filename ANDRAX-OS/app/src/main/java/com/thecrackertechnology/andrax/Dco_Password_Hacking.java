package com.thecrackertechnology.andrax;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;

import com.thecrackertechnology.dragonterminal.bridge.Bridge;

public class Dco_Password_Hacking extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);



        setContentView(R.layout.dco_password_hacking);

        CardView cardviewhydra = findViewById(R.id.card_view_hydra);
        CardView cardviewncrack = findViewById(R.id.card_view_ncrack);
        CardView cardviewjohn = findViewById(R.id.card_view_john);
        CardView cardviewhashcat = findViewById(R.id.card_view_hashcat);
        CardView cardviewciscopwdecrypt = findViewById(R.id.card_view_ciscopwdecrypt);
        CardView cardviewhashboy = findViewById(R.id.card_view_hashboy);
        CardView cardviewcrunch = findViewById(R.id.card_view_crunch);
        CardView cardviewmaskprocessor = findViewById(R.id.card_view_maskprocessor);
        CardView cardviewcewl = findViewById(R.id.card_view_cewl);
        CardView cardviewmassh = findViewById(R.id.card_view_massh);
        CardView cardviewacccheck = findViewById(R.id.card_view_acccheck);
        CardView cardviewsshauditor = findViewById(R.id.card_view_ssh_auditor);
        CardView cardviewbopscrk = findViewById(R.id.card_view_bopscrk);
        CardView cardviewpskcrack = findViewById(R.id.card_view_pskcrack);
        CardView cardviewcr3d0v3r = findViewById(R.id.card_view_cr3d0v3r);


        cardviewhydra.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("hydra");

            }
        });

        cardviewncrack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("ncrack");

            }
        });

        cardviewjohn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("john");

            }
        });

        cardviewhashcat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("hashcat -h");

            }
        });

        cardviewciscopwdecrypt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("cisco_pwdecrypt -h");

            }
        });

        cardviewhashboy.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("hashboy");

            }
        });

        /**
         *
         * Help me, i'm dying...
         *
         **/

        cardviewcrunch.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("crunch");

            }
        });

        cardviewmaskprocessor.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("maskprocessor --help");

            }
        });

        cardviewcewl.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("cewl -h");

            }
        });

        cardviewmassh.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("sudo massh-enum --help");

            }
        });

        cardviewacccheck.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("acccheck");

            }
        });


        cardviewsshauditor.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("ssh-auditor");

            }
        });

        cardviewbopscrk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("bopscrk");

            }
        });

        cardviewpskcrack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("psk-crack");

            }
        });

        cardviewcr3d0v3r.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("cr3d0v3r -h");

            }
        });


    }

    public void run_hack_cmd(String cmd) {

        Intent intent = Bridge.createExecuteIntent(cmd);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);

    }

    @Override
    public void onPause() {

        super.onPause();
        finish();
    }
}
